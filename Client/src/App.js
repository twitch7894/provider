import React, { useState, useEffect } from "react";
import Nav from "./components/Nav";
import Signup from "./containers/Signup";
import Login from "./containers/Login";
import Axios from "axios";
import {
  setToken,
  getToken,
  initAxiosInterceptors,
  setUsername
} from "./helpers/auth-helpers";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Error from "./components/Error";
import Providers from "./containers/Providers";
import { ENDPOINTS } from "./config/endpoints";
import Details from "./containers/Details";
import Validate from "./components/valid";

initAxiosInterceptors();

export default function App() {
  const [user, setUser] = useState(null);
  const [error, setError] = useState(null);
  const [valid, setValid] = useState(null)

  useEffect(() => {
    loadUser();
  }, []);

  async function loadUser() {
    if (getToken()) {
      setUser(getToken());
    }
  }
  async function login(username, password) {
    const { data } = await Axios.post(ENDPOINTS.LOGIN, {
      username,
      password
    });
    console.log("data", data);
    setUser(data.token);
    setUsername(data.username);
    setToken(data.token);
  }

  async function signup(user) {
    const { data } = await Axios.post(ENDPOINTS.SIGNUP, user);
    setValid(data);
  }

  function showError(mensaje) {
    setError(mensaje);
  }

  function CloseError() {
    setError(null);
  }
  function showValidate(mensaje) {
    setValid(mensaje);
  }

  function CloseValidate() {
    setValid(null);
  }

  return (
    <Router>
      <Nav />
      {user !== null ? (
        <LoginRoutes  showValidate={showValidate} />
      ) : (
        <LogoutRoutes login={login} signup={signup} showError={showError} showValidate={showValidate} />
      )}
      <Error message={error} CloseError={CloseError} />
      <Validate message={valid} Close={CloseValidate}/>
    </Router>
  );

  function LoginRoutes() {
    return (
      <Switch>
        <Route 
        exact 
        path="/" component={() => <Providers />} />
        <Route path="/provider/:id" exact component={() => <Details />} />
      </Switch>
    );
  }

  function LogoutRoutes({ login, showError, showValidate }) {
    return (
      <Switch>
        <Route
          path="/login"
          exact
          render={props => (
            <Login {...props} login={login} showError={showError} />
          )}
        />
        <Route
          path="/"
          exact
          render={props => (
            <Signup {...props} signup={signup} showError={showError} showValidate={showValidate} />
          )}
        />
      </Switch>
    );
  }
}
