/**
 *  ENDPOINTS PROVIDER
 */
const HOST = "http://localhost:3000";

export const ENDPOINTS = {
  LOGIN: `${HOST}/users/login`,
  SIGNUP: `${HOST}/users/signup`,
  ACCOUNT: id => `${HOST}/providers/owner/${id}`,
  PROVIDERS: `${HOST}/providers`,
  DELETEPROVIDER: id => `${HOST}/providers/${id}`,
  UPDATEPROVIDER: id => `${HOST}/providers/${id}`,
  PROVIDERSID: id => `${HOST}/providers/${id}`
};
