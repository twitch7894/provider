import React from "react";


export default function Error({ message, CloseError }) {
  if (!message) {
    return null;
  }

  return (
    <div className="ErrorContainer" rol="alert">
      <div className="Error_inner">
        <span className="block">{message}</span>
        <button className="Error__icon" onClick={CloseError}>
          (x)
        </button>
      </div>
    </div>
  );
}
