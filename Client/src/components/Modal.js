import React from "react";
import ReactDOM from "react-dom";

const Modal = ({
  isShowing,
  hide,
  providers,
  handleInputChange,
  handleSubmit,
  data
}) =>
  isShowing
    ? ReactDOM.createPortal(
        <React.Fragment>
          <div className="modal-overlay" />
          <div
            className="modal-wrapper"
            aria-modal
            aria-hidden
            tabIndex={-1}
            role="dialog"
          >
            <div className="modal">
              <div className="modal-header">
                <button
                  type="button"
                  className="modal-close-button"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={() => hide(false)}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="container">
                <h1 style={{ color: "white" }}>Provider</h1>
                <form className="form">
                  <input
                    type="string"
                    name="name"
                    placeholder="name"
                    className="Form__field"
                    onChange={e => handleInputChange(e)}
                    value={providers.name}
                  />
                  <input
                    type="string"
                    name="cuit"
                    placeholder="cuit"
                    className="Form__field"
                    maxLength="11"
                    onChange={e => handleInputChange(e)}
                    value={providers.cuit}
                  />
                  <input
                    type="string"
                    name="email"
                    placeholder="email"
                    className="Form__field"
                    onChange={e => handleInputChange(e)}
                    value={providers.email}
                  />
                  <input
                    type="string"
                    name="direction"
                    placeholder="direction"
                    className="Form__field"
                    onChange={e => handleInputChange(e)}
                    value={providers.direction}
                  />
                  <input
                    type="string"
                    name="phone"
                    placeholder="phone"
                    className="Form__field"
                    onChange={e => handleInputChange(e)}
                    value={providers.phone}
                  />
                  <button className="Form__submit" onClick={handleSubmit}>
                    data
                  </button>
                </form>
              </div>
            </div>
          </div>
        </React.Fragment>,
        document.body
      )
    : null;

export default Modal;
