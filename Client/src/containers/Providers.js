import React, { useState, useEffect } from "react";
import Axios from "axios";
import Loading from "../components/Loading";
import { ENDPOINTS } from "../config/endpoints";
import Modal from "../components/Modal";
import { initAxiosInterceptors } from "../helpers/auth-helpers";
import img from "../imgs/reload-icon.png";
import Error from "../components/Error";
import Validate from "../components/valid"
import { Link } from "react-router-dom";

initAxiosInterceptors();

export default function Providers() {
  const [load, setLoad] = useState(true);
  const [info, setInfo] = useState(null);
  const [error, setError] = useState(null);
  const [valid, setValid] = useState(null)
  const [isShowing, setIsShowing] = useState(false);
  const [change, setChange] = useState({
    type: "post",
    endpoint: ENDPOINTS.PROVIDERS
  });
  const [providers, setProviders] = useState({
    name: "",
    cuit: "",
    email: "",
    direction: "",
    phone: ""
  });

  useEffect(() => {
    getInfo();
  }, []);

  async function getInfo() {
    const { data } = await Axios.get(
      ENDPOINTS.ACCOUNT(localStorage.USERNAME_KEY)
    );
    setInfo(data);
    console.log("name info", info)
    setLoad(false);
  }
  async function deleteInfo(id) {
    try {
      const data = await Axios.delete(ENDPOINTS.DELETEPROVIDER(id));
    } catch (error) {
      showError(error.response.data);
      setIsShowing(true);
    }
  }
  async function updateInfo(e) {
    setProviders({
      name: e.name,
      cuit: e.cuit,
      email: e.email,
      direction: e.direction,
      phone: e.phone
    });
    setChange({
      type: "put",
      endpoint: ENDPOINTS.UPDATEPROVIDER(e._id)
    });
    setIsShowing(true);
  }
  async function addInfo() {
    setProviders({
      name: "",
      cuit: "",
      email: "",
      direction: "",
      phone: ""
    });
    setChange({
      type: "post",
      endpoint: ENDPOINTS.PROVIDERS
    });
    setIsShowing(true);
  }
  
  function showError(mensaje) {
    setError(mensaje);
  }

  function CloseError() {
    setError(null);
  }

  function showValidate(mensaje) {
    setValid(mensaje);
  }

  function CloseValidate() {
    setValid(null);
  }

  const handleInputChange = e => {
    setProviders({ ...providers, [e.target.name]: e.target.value });
    console.log("providers", providers);
  };

  const handleSubmit = async e => {
    e.preventDefault();

    try {
      const data = await Axios[change.type](change.endpoint, providers);
      showValidate(data.statusText)
      console.log("test data",data.statusText)
      setIsShowing(false);
    } catch (error) {
      showError(error.response.data);
      setIsShowing(true);
    }
  };

  return (
    <React.Fragment>
      <Error message={error} CloseError={CloseError} />
      <Validate message={valid} Close={CloseValidate} />
      <div style={{ marginTop: "110px" }}>
        <button className="ButtonModal" onClick={addInfo}>
          ADD
        </button>
        <button onClick={getInfo} className="reload">
          <img src={img} width={35} />
        </button>
        <div className="table">
          {load ? (
            <Loading />
          ) : (
            <div className="container">
              <h1 style={{ color: "black" }}>Resume</h1>
              <table className="rwd-table">
                <tbody>
                  <tr>
                    <th className="table-type">Name</th>
                    <th className="table-type">Cuit</th>
                    <th className="table-type">Email</th>
                    <th className="table-type">Direction</th>
                    <th className="table-type">Phone</th>
                    <th className="table-type">Options</th>
                  </tr>
                  {info.map(e => (
                    <tr key={e._id}>
                      <td data-th="Name">{e.name}</td>
                      <td data-th="Cuit">{e.cuit}</td>
                      <td data-th="email">{e.email}</td>
                      <td data-th="direction">{e.direction}</td>
                      <td data-th="phone">{e.phone}</td>
                      <td data-th="options">
                        <button onClick={() => updateInfo(e)}>edit</button>
                        <button onClick={() => deleteInfo(e._id)}>
                          delete
                        </button>
                        <Link to={`/provider/${e._id}`}>view</Link>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        </div>
        <Modal
          isShowing={isShowing}
          hide={setIsShowing}
          handleSubmit={handleSubmit}
          handleInputChange={handleInputChange}
          providers={providers}
          data={change}
        />
      </div>
    </React.Fragment>
  );
}
