import React, { useState, useEffect } from "react";
import Axios from "axios";
import { ENDPOINTS } from "../config/endpoints";
import Loading from "../components/Loading";
import { useParams } from "react-router-dom";

export default function Details() {
  let { id } = useParams();
  const [load, setLoad] = useState(true);
  const [item, setItem] = useState(null);

  useEffect(() => {
    getInfo();
  }, []);

  async function getInfo() {
    const { data } = await Axios.get(ENDPOINTS.PROVIDERSID(id));
    setItem(data);
    console.log("info", item);
    setLoad(false);
  }

  return (
    <div style={{ marginTop: "110px" }}>
      <div className="table">
        {load ? (
          <Loading />
        ) : (
          <div className="container">
            <h1 style={{ color: "black" }}>Resume</h1>
            <table className="rwd-table">
              <tbody>
                <tr>
                  <th className="table-type">Name</th>
                  <th className="table-type">Cuit</th>
                  <th className="table-type">Email</th>
                  <th className="table-type">Direction</th>
                  <th className="table-type">Phone</th>
                </tr>
                <tr key={item._id}>
                  <td data-th="Name">{item.name}</td>
                  <td data-th="Cuit">{item.cuit}</td>
                  <td data-th="email">{item.email}</td>
                  <td data-th="direction">{item.direction}</td>
                  <td data-th="phone">{item.phone}</td>
                </tr>
              </tbody>
            </table>
          </div>
        )}
      </div>
    </div>
  );
}
