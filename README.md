## Quick Overview

```sh
git clone https://gitlab.com/twitch7894/exercise/-/tree/master
```
The Backend
requirement to install and run MongoDB.
cd exercise/Server

RUN yarn
```
Then open [http://localhost:3000/](http://localhost:3000/) to see your app.<br>
when you’re ready to deploy to development, bundle with `yarn start`.
```
The Frontend

cd exercise/Client

RUN yarn
```
Then open [http://localhost:8080/](http://localhost:8080/) to see your app.<br>
when you’re ready to deploy to development, bundle with `yarn dev`.

PD: 
At the beginning of the session you have to register.
to enter
with user and password you can login
```