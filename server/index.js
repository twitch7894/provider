const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const mongoose = require('mongoose')
const cors = require('cors')
const usersRouter = require('./api/resources/users/users.routes')
const logger = require('./utils/logger')
const authJWT = require('./api/libs/auth')
const config = require('./config')
const errorHandler = require('./api/libs/errorHandler')
const ProvidersRouter = require('./api/resources/providers/providers.routes')
const passport = require('passport')
passport.use(authJWT)

mongoose.connect('mongodb://127.0.0.1:27017/provider')
mongoose.connection.on('error', () => {
  logger.error('Connection to mongodb failed')
  process.exit(1)
})
mongoose.set('useFindAndModify', false)

const app = express()
app.use(bodyParser.json())
app.use(morgan('short', {
  stream: {
    write: message => logger.info(message.trim())
  }
}))

app.use(passport.initialize())
app.use(cors())
app.use('/users', usersRouter)
app.use('/providers', ProvidersRouter)

app.use(errorHandler.processErrorsDB)
if (config.Environment === 'prod') {
  app.use(errorHandler.errorsProduction)
} else {
  app.use(errorHandler.errorsDeveloper)
}

const server = app.listen(config.port, () => {
  logger.info(`Listening in the port ${config.port}.`)
})

module.exports = {
  app, server
}