const Environment = process.env.NODE_ENV || 'development'

const configurationBase = {
  jwt: {},
  port: 3000,
  DeleteLogs: false
}

let EnvironmentSetting= {}

switch (Environment) {
  case 'develop':
  case 'dev':
  case 'development':
    EnvironmentSetting = require('./dev')
    break
  case 'production':
  case 'prod':
    EnvironmentSetting = require('./prod')
    break
  case 'test':
    EnvironmentSetting = require('./test')
    break
  default:
    EnvironmentSetting = require('./dev')
}

module.exports = {
  ...configurationBase,
  ...EnvironmentSetting
}