const _ = require('underscore')
const bcrypt = require('bcrypt')
const passportJWT = require('passport-jwt')

const log = require('./../../utils/logger')
const config = require('../../config')
const userController = require('../resources/users/users.controller')

let jwtOptions = {
  secretOrKey: config.jwt.secret,
  jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken()
}

module.exports = new passportJWT.Strategy(jwtOptions, (jwtPayload, next) => {
  userController.getUser({ id: jwtPayload.id })
    .then(user => {
      if (!user) {
        log.info(`JWT token is not valid. User with id ${jwtPayload.id} not exist.`)
        next(null, false)
        return
      }

      log.info(`User ${user.username} Supply a valid token. Authentication completed.`)
      next(null, {
        username: user.username,
        id: user.id
      })
    })
    .catch(err => {
      log.error("Error trying to validate a token.", err)
      next(err)
    })
})