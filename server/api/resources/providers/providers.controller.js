const Providers = require('./providers.model')

function createProviders(providers, owner) {
  return new Providers({
    ...providers,
    owner
  }).save()
}

function getProviders() {
  return Providers.find({})
}

function getProvider(id) {
  return Providers.findById(id)
}

function userExists(cuit, email) {
  return new Promise((resolve, reject) => {
    Providers.find().or([ { 'cuit': cuit }, { 'email': email } ])
      .then(users => {
        resolve(users.length > 0)
      })
      .catch(err => {
        reject(err)
      })
  })
}

function getProviderOwner(id) {
    return Providers.find({ owner: id })
}

function deleteProvider(id) {
  return Providers.findByIdAndRemove(id)
}

function replaceProvider(id, providers, username) {
  return Providers.findOneAndUpdate({ _id: id }, {
    ...providers,
    owner: username
  }, {
    new: true 
  })
}

module.exports = {
  createProviders,
  getProviders,
  getProvider,
  deleteProvider,
  replaceProvider,
  getProviderOwner,
  userExists
}