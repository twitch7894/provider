class ProvidersDoesNotExist extends Error {
  constructor(message) {
    super(message)
    this.message = message || 'Providers does not exist. Operation cannot be completed.'
    this.status = 404
    this.name = 'ProviderDoesNotExist'
  }
}

class CUITDataAlreadyInUse extends Error {
  constructor(message) {
    super(message)
    this.message = message || 'The cuit is already associated with an account.'
    this.status = 409
    this.name = 'CUITDataAlreadyInUse'
  }
}

class UserDoesNotOwn extends Error {
  constructor(message) {
    super(message)
    this.message = message || 'You are not the owner of the Provider. Operation cannot be completed.'
    this.status = 401
    this.name = 'UserDoesNotOwn'
  }
}

module.exports = {
  ProvidersDoesNotExist,
  UserDoesNotOwn,
  CUITDataAlreadyInUse
}