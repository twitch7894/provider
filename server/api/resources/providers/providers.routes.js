const express = require('express')
const _ = require('underscore')
const passport = require('passport')

const validarProviders = require('./providers.validate')
const log = require('./../../../utils/logger')
const ProvidersController = require('./providers.controller')
const processErrors = require('../../libs/errorHandler').processErrors
const { ProvidersDoesNotExist, UserDoesNotOwn, CUITDataAlreadyInUse } = require('./providers.error')

const jwtAuthenticate = passport.authenticate('jwt', { session: false })
const ProvidersRouter = express.Router()

function validateId(req, res, next) {
  let id = req.params.id
  // regex = regular expressions
  if (id.match(/^[a-fA-F0-9]{24}$/) === null) {
    res.status(400).send(`the id [${id}] provided in the URL is not valid`)
    return
  }
  next()
}

ProvidersRouter.get('/', processErrors((req, res) => {
  return ProvidersController.getProviders()
    .then(providers => {
      res.json(providers)
    })
}))

ProvidersRouter.get('/owner/:id', processErrors((req, res) => {
    let id = `${req.params.id}`
    return ProvidersController.getProviderOwner(id)
      .then(providers => {
        res.json(providers)
      })
  }))

  ProvidersRouter.post('/', [jwtAuthenticate, validarProviders], processErrors((req, res) => {
    /*   ProvidersController.userExists(req.body.cuit, req.body.email)
      .then(userExists => {
        if (userExists) {
          log.warn(`Email [${req.body.email}] o cuit [${req.body.cuit}] they already exist in the database`)
        res.status(409).send(`Email [${req.body.email}] o cuit [${req.body.cuit}] they already exist in the database`)
        } 
      }) */
      
      return ProvidersController.createProviders(req.body, req.user.username)
      .then(providers => {
        log.info("providers added to the providers collection", providers)
        res.status(201).json(providers)
      })

}))

ProvidersRouter.get('/:id', validateId, processErrors((req, res) => {
  let id = req.params.id
  return ProvidersController.getProvider(id)
    .then(providers => {
      if (!providers) throw new ProvidersDoesNotExist(`providers with id [${id}] not exist.`)
      res.json(providers)
    })
}))

ProvidersRouter.put('/:id', [jwtAuthenticate, validarProviders], processErrors(async (req, res) => {
  let id = req.params.id
  let requestUser = req.user.username
  let ProvidersReplace

  ProvidersReplace = await ProvidersController.getProvider(id)

  if (!ProvidersReplace) throw new ProvidersDoesNotExist(`Providers with id [${id}] does not exist.`)

  if (ProvidersReplace.owner !== requestUser) {
    log.warn(`User [${requestUser}] is not the owner of Providers with id [${id}]. real owner is [${ProvidersReplace.owner}]. Request will not be processed`)
    throw new UserDoesNotOwn(`You are not the owner of the Providers with id [${id}]. You can only modify Providers created by you.`)
  }

  ProvidersController.replaceProvider(id, req.body, requestUser)
    .then(providers => {
      res.json(providers)
      log.info(`Providers with id [${id}] replaced with new Providers`, providers)
    })
}))
  
ProvidersRouter.delete('/:id', [jwtAuthenticate, validateId], processErrors(async (req, res) => {
  let id = req.params.id
  let ProvidersDelete

  ProvidersDelete = await ProvidersController.getProvider(id)
  
  if (!ProvidersDelete) {
    log.info(`providers with id [${id}] does not exist. Nothing to erase`)
    throw new ProvidersDoesNotExist(`providers with id [${id}] does not exist. Nothing to erase`)
  }

  let AuthenticatedUser = req.user.username
  if (ProvidersDelete.owner !== AuthenticatedUser) {
    log.info(`User [${AuthenticatedUser}] is not the owner of providers with id [${id}]. real owner is [${ProvidersDelete.owner}]. Request will not be processed`)
    throw new UserDoesNotOwn(`You are not the owner of the providers with id [${id}]. You can only delete providers created by you.`)
  }

  let ProvidersDeleted = await ProvidersController.deleteProvider(id)
  log.info(`Transactions with id [${id}] was deleted`)
  res.json(ProvidersDeleted)
}))

module.exports = ProvidersRouter