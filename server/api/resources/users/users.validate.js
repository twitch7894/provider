const Joi = require('joi')
const log = require('./../../../utils/logger')

const blueprintUser = Joi.object().keys({
  username: Joi.string().alphanum().min(3).max(30).required(),
  password: Joi.string().min(6).max(200).required(),
  email: Joi.string().email().required()
})

let validateUser = (req, res, next) => {
  const result = Joi.validate(req.body, blueprintUser, { abortEarly: false, convert: false })

  if (result.error === null) {
    next()
  } else {
    log.info("Product failed validation", result.error.details.map(error => error.message))
    res.status(400).send("User information does not meet the requirements. The name of the User must be alafanúmerico and have between 3 and 30 characters. The password must be between 6 and 200 characters. Make sure the email is valid.")
  }
}

const blueprintLogin = Joi.object().keys({
  username: Joi.string().required(),
  password: Joi.string().required()
})

let validateLogin = (req, res, next) => {
  const result = Joi.validate(req.body, blueprintLogin, { abortEarly: false, convert: false })
  if (result.error === null) {
    next()
  } else {
    res.status(400).send("Login failed. You must specify the username and password of the User. Both must strings.")
  }
}

module.exports = {
  validateLogin,
  validateUser
}