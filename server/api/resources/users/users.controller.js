const User = require('./users.model')

function getUsers() {
  return User.find({})
}

function CreateUser(user, hashedPassword) {
  return new User({
    ...user,
    password: hashedPassword
  }).save()
}

function userExists(username, email) {
  return new Promise((resolve, reject) => {
    User.find().or([ { 'username': username }, { 'email': email } ])
      .then(users => {
        resolve(users.length > 0)
      })
      .catch(err => {
        reject(err)
      })
  })
}

function getUser({
  username: username,
  id: id
}) {
  if (username) return User.findOne({ username: username })
  if (id) return User.findById(id)
  throw new Error("Function get User of the controller was called without specifying username or id.")
}

module.exports = {
  getUsers,
  CreateUser,
  userExists,
  getUser
}